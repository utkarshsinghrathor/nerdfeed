//
//  CustomTableViewCell.m
//  Nerdfeed
//
//  Created by Mac-6 on 31/12/15.
//  Copyright (c) 2015 com.utkarsh. All rights reserved.
//

#import "CustomTableViewCell.h"

@implementation CustomTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {

        NSLog(@"Calling the custom initilisation part of teh code ofr the UITableviewcell") ;
    }
    
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
    NSLog(@"%@ %@ ",NSStringFromSelector(_cmd), [self class] );
          
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    
    NSLog(@"Calling the custom method") ;
    // Configure the view for the selected state
}

@end
