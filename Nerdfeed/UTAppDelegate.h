//
//  UTAppDelegate.h
//  Nerdfeed
//
//  Created by Mac-6 on 31/12/15.
//  Copyright (c) 2015 com.utkarsh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UTAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
