//
//  main.m
//  Nerdfeed
//
//  Created by Mac-6 on 31/12/15.
//  Copyright (c) 2015 com.utkarsh. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UTAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([UTAppDelegate class]));
    }
}
