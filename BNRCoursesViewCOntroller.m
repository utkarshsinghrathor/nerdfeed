//
//  BNRCoursesViewCOntroller.m
//  Nerdfeed
//
//  Created by Mac-6 on 31/12/15.
//  Copyright (c) 2015 com.utkarsh. All rights reserved.
//

#import "BNRCoursesViewCOntroller.h"

@interface BNRCoursesViewCOntroller()

@property (nonatomic) NSURLSession *session ;

@end


@implementation BNRCoursesViewCOntroller


-(void)fetchFeed
{
    NSString *requestString = @"http://bookapi.bignerdraqnch.com/courses.json" ;
    
    NSURL *url = [NSURL URLWithString:requestString] ;
    
    NSURLRequest * req = [NSURLRequest requestWithURL:url] ;
    
    
    NSURLSessionDataTask *dataTask = [self.session dataTaskWithRequest:req completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if( nil == error)
        {
            NSLog(@"No errors have been found") ;
            
            NSString *json = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] ;
            
            NSLog(@"%@",json ) ;
        }
        else
        {
            NSLog(@"There were some errors in the request fulfillment") ;
            
        }
        
    } ] ;
    
    [dataTask resume] ;
    
}

-(instancetype) initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style] ;
    if( self != nil )
    {
        self.navigationItem.title = @"Coursesfrom BNR" ;
        
        
        NSURLSessionConfiguration *config = [ NSURLSessionConfiguration  defaultSessionConfiguration] ;
        
        _session = [NSURLSession sessionWithConfiguration:config delegate:nil delegateQueue:nil] ;
        
    }

    return self ;
}




-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
    
    
}


-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"] ;
    if( cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"cell"] ;
        
    }
    cell.textLabel.text  = [NSString stringWithFormat:@"String %u",indexPath.row ] ;
    cell.detailTextLabel.text = @"More Value" ;
    
    

    return cell ;
}

@end
